# WorkTeam

Aplicación para organizar equipos de trabajo


## <div id= "Description"></div> Configuración de la Aplicación


* Se hace uso de docker para generar el espacio de trabajo de la aplicación.
* Es necesario configurar crear archivo .env para pasar las variables de entorno
* Se adjunta un archivo .env.example para probar en local, por favor cambie el nombre a .env para probar en local


## <div id= "Content"></div> Contenido

- [Configuración de la Aplicación](#Description)
- [Contenido](#Content)
- [Correr docker-compose](#Started)
- [Modelo base de datos](#Contributing)
- [Tecnologias](#Tecnologias)
- [Recursos](#Recursos)



## <div id= "Started"></div> Correr docker-compose

Ahora puede seguir con:

```sh
$ docker-compose build
```

El cual se encargara de generar la imagen donde correra el proyecto de Django

El docker-compose necesita de un volumen externo para montar la base de datos. el volumen externo es llamado db_data. Utilice el siguiente comando antes de correr el docker-compose:

```sh
$ docker volume create --name db_data
```

El siguiente paso es:
```sh
$ docker-compose up
```

Luego hay que hacer las migraciones correspondientes en la base de datos.
Desde otro terminal o poniendo la ejecución del servidor en segundo plano con el argumento -d se ejecuta el siguiente comando
```sh
$ docker exec -it workteam_web_1 bash
```
Con esto abrimos un bash dentro del contenedor y desde allí haremos la migraciones en la base de datos.
```sh
/code# python manage.py makemigrations

/code# python manage.py migrate
```

Despues crearemos el super usuario el cual podra entrar al panel administrativo, desde alli podra crear y manipular los diferentes items que se pueden crear en el proyecto.
```sh
/code# python manage.py createsuperuser
```

Con esto ya pondremos en funcionamiento el servicio y se puede acceder al sitio ingresando la dirección 127.0.0.1:8000

# Django REST framework API V1
```sh
https://127.0.0.1/api/v1/
```

## Se puede acceder a la siguientes Endpoints

| Endpoint                       | Http verb  | Descripción                       |
|--------------------------------|------------|-----------------------------------|
| /teams                         | GET        | Lista todos los equipos           |
| /teams                         | POST       | Agrega varios equipos             |
| /teams/<int:team_id>/          | GET        | Muestra un equipo                 |
| /teams/<int:team_id>/update/   | PUT, PATCH | Actualiza el nombre de un equipo  |
| /teams/<int:team_id>/          | DELETE     | Borra un equipo                   |
| /teams/create/                 | POST       | Crea un nuevo equipo              |
| /members                       | GET        | Lista todos los miembros          |
| /members                       | POST       | Agrega varios miembros            |
| /members/<int:team_id>/        | GET        | Muesta un miembro                 |
| /members/<int:team_id>/update/ | PUT, PATCH | Actualiza el nombre de un miembro |
| /members/<int:team_id>/        | DELETE     | Borra un equipo                   |
| /members/create/               | POST       | Crea un nuevo miembro             |
| /users/                        | GET        | Lista todos los usuarios          |
| /users/                        | POST       | Agrega varios usuarios            |
| /users/<int:team_id>/          | GET        | Muestra un usuario                |
| /members/<int:team_id>/update/ | PUT, PATCH | Actualiza datos del usuario       |
| /members/<int:team_id>/        | DELETE     | Borra un usuario                  |
| /members/create/               | POST       |                                   |




### Como unir múltiples miembros al mismo equipo.

{
    "user_id": [user_id_1, user_id_2, user_id_N],
    "team_id": ID_equipo
}

### Como agregar múltiples usuarios.

[
{
    "username": "1",
    "email": "1@gmail.com"
},
{
    "username": "2",
    "email": "2@gmail.com"
},
{
    "username": "3",
    "email": "3@gmail.com"
}
]

### Como agregar múltiples equipos.
[
{
    "name": "Equipo_1"
},
{
    "name": "Equipo_2"
},
{
    "name": "Equipo_3"
}
]

### Conceder los permisos a las carpetas creadas mediante docker

Cuando se crean nuevas aplicaciones desde en manage.py de django, los permisos para acceder a estos archivos son insuficientes para el host, entonces con el siguiente comando se le asignan los correspondientes permisos.

´´´sh
$ sudo chown -R $USER:$USER .
´´´



## <div id= "Contributing"></div> Modelo base de datos
[![Modelo](https://gitlab.com/purohr/work-team/-/blob/master/modelo.png)](https://gitlab.com/purohr/work-team/-/blob/master/modelo.png)

## <div id= "Tecnologias"></div> Tecnologias

* Python
* Django
* Django RestApi
* Docker
* Pillow
* Postgresql

## <div id= "Recursos"></div> Recursos

* [ListCreateApiView](https://www.agiliq.com/blog/2019/05/django-rest-framework-listcreateapiview/)
* [Personalizando la autenticación en Django](https://docs.djangoproject.com/en/3.1/topics/auth/customizing/)
* [Modelos en django](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Models)
* [Vistas basadas en clases](https://www.django-rest-framework.org/api-guide/views/)

### Debug Django

comando apr debugear django,  
´´´sh
import ipdb; ipdb.set_trace()
´´´