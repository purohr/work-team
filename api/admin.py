from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from api.models import Team, Member, User

# Register your models here.
# admin.site.register(User)

from api.models import User, Team, Member

class CustomUserAdmin(UserAdmin):
	list_display = ('email', 'username', 'first_name', 'last_name', 'is_staff')
	list_filter = ('is_staff', 'created_at', 'updated_at')
	search_fields = ('username', 'first_name', 'last_name')

admin.site.register(User, CustomUserAdmin)


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
	list_display = ('name', 'image', 'created_at')	
	list_filter = ('name', 'created_at')

@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
	list_display = ('user_id', 'team_id', 'date_of_admission')
	list_filter = ('team_id', 'date_of_admission')
