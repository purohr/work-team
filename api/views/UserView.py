
from rest_framework.response import Response
from rest_framework.views import APIView
from api.serializers import UserSerializer
from api.models import User
from django.http import Http404
from rest_framework import status
from rest_framework.generics import UpdateAPIView, CreateAPIView


class Users(APIView):

	def get(self, request):
		users = User.objects.all()
		serializer = UserSerializer(users, many=True)
		return Response(serializer.data)


	def post(self, request):
		serializer = UserSerializer(data=request.data, many=True)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserDetail(APIView):
			"""Detail of a User, method get and Delete"""

			def get(self, request, user_id):
				try:
					user = User.objects.get(pk=user_id)
				except User.DoesNotExist:
					raise Http404
				serializer = UserSerializer(user)
				return Response(serializer.data)

			def delete(self, request, user_id):
				try:
					user = User.objects.get(pk=user_id)
				except User.DoesNotExist:
					raise Http404
				user.delete()
				return Response(status=status.HTTP_204_NO_CONTENT)

			

class UserUpdateView(UpdateAPIView):
	"""docstring for UserUpdate, method Update restframework """
	queryset = User.objects.all()
	serializer_class = UserSerializer

class UserCreateView(CreateAPIView):
	"""docstring for UserCreateView"""
	serializer_class = UserSerializer
	queryset = User.objects.all()

	def post(self, request):
		serializer = self.serializer_class(data=request.data)

		if serializer.is_valid():
			serializer.save()
			return Response({'message': 'Usuario creado correctamente'}, status = status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)