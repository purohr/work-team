from rest_framework.response import Response
from rest_framework.views import APIView
from api.serializers import MemberSerializer
from api.models import Member
from django.http import Http404
from rest_framework import status
from rest_framework.generics import UpdateAPIView, CreateAPIView

class Members(APIView):
	"""docstring for Member: implement get and post method"""
	
	def get(self, request):
		members = Member.objects.all()
		serializer = MemberSerializer(members, many=True)
		return Response(serializer.data)

	def post(self, request):
		# import ipdb; ipdb.set_trace()
		# print('Consola')
		for u in request.data['user_id']:
			serializer = MemberSerializer(
				data = {
					"team_id":request.data['team_id'],
					"user_id":u,
					}
				)
			if serializer.is_valid():
				serializer.save()
			else:
				return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		return Response(serializer.data, status=status.HTTP_201_CREATED)
		# serializer = MemberSerializer(data=request.data)
		# print(request.data)
		# if serializer.is_valid():
		# 	serializer.save()
		# 	return Response(serializer.data, status=status.HTTP_201_CREATED)
		# return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class MemberDetail(APIView):
	"""docstring for MemberDetail"""
	def get(self, request, member_id):
		try:
			member = Member.objects.get(pk=member_id)
		except Member.DoesNotExist:
			raise Http404
		serializer = MemberSerializer(member)
		return Response(serializer.data)

	def delete(self, request, member_id):
		try:
			member = Member.objects.get(pk=member_id)
		except  Member.DoesNotExist:
			raise Http404
		member.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)

class MemberUpdateView(UpdateAPIView):
	"""docstring for MemberUpdateview"""
	queryset = Member.objects.all()
	serializer_class = MemberSerializer

class MemberCreateView(CreateAPIView):
	"""docstring for MemberCreateView"""
	serializer_class = MemberSerializer
	queryset = Member.objects.all()

	def post(self, request):
		serializer = self.serializer_class(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response({'Message': 'Miembro creado correctamente'}, status = status.HTTP_201_CREATED)

				
			
						





						



		
