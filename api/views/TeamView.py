
from rest_framework.response import Response
from rest_framework.views import APIView
from api.serializers import TeamSerializer, MemberSerializer
from api.models import Team, Member
from django.http import Http404
from rest_framework import status
from rest_framework.generics import UpdateAPIView, CreateAPIView
#send mail
from django.core.mail import send_mail



class Teams(APIView):

	def get(self, request):
		""" GET /api/v1/teams/"""		
		return Response(self.teams_with_members())


	def post(self, request):
		serializer = TeamSerializer(data=request.data, many=True)
		if serializer.is_valid():
			serializer.save()
			send_mail(
       			'Alerta de creación de equipo',
       			'se ha creado un equipo llamado {0}'.format(request.data['name']),
       			'workteam@example.com',
        		['diego683@gmail.com'],
        		fail_silently=False,
       			)
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	def teams_with_members(self):
		teams = Team.objects.all()
		data = []
		for team in teams:
			members = Member.objects.filter(team_id=team.id)
			serializer_team = TeamSerializer(team)
			serializer_member = MemberSerializer(members, many=True)
			data.append(serializer_team.data)
			data[len(data)-1]['members'] = serializer_member.data



class TeamDetail(APIView):
	"""Detail of a team, method get and Delete"""

	def get(self, request, team_id):
		try:
			team = Team.objects.get(pk=team_id)
		except Team.DoesNotExist:
			raise Http404
		serializer = TeamSerializer(team)
		return Response(serializer.data)

	def delete(self, request, team_id):
		try:
			team = Team.objects.get(pk=team_id)
		except Team.DoesNotExist:
			raise Http404
		team.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)

			

class TeamUpdateView(UpdateAPIView):
	"""docstring for TeamUpdate, method Update restframework """
	queryset = Team.objects.all()
	serializer_class = TeamSerializer

class TeamCreateView(CreateAPIView):
	"""docstring for TeamCreateView"""
	serializer_class = TeamSerializer
	queryset = Team.objects.all()

	def post(self, request):
		serializer = self.serializer_class(data=request.data)

		if serializer.is_valid():
			serializer.save()
			send_mail(
       			'Alerta de creación de equipo',
       			'se ha creado un equipo llamado {0}'.format(request.data['name']),
       			'workteam@example.com',
        		['diego683@gmail.com'],
        		fail_silently=False,
       			)
			return Response({'message': 'Equipo creado correctamente'}, status = status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		
