from rest_framework import serializers
from api.models import Member


class MemberSerializer(serializers.ModelSerializer):
    #nombre = serializers.CharField()
    
    class Meta:
        model = Member
        fields = [
        	'user_id',            
            'team_id',
            'date_of_admission',
            'id',
        ]