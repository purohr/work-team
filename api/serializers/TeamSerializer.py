from rest_framework import serializers
from api.models import Team, Member
from api.serializers import MemberSerializer

#imagen
import base64
from django.conf import settings
import os


class TeamSerializer(serializers.ModelSerializer):
	#nombre = serializers.CharField()

    image = serializers.SerializerMethodField('encode_thumbnail')
    
    def encode_thumbnail(self, team):
        with open(os.path.join(settings.MEDIA_ROOT, team.image.name), "rb") as image_file:
            return base64.b64encode(image_file.read())

   #  def to_representation(self,instance):
   #      #user = Member.objects.filter(team=instance['id'])
   #      return {
			# 'id': instance['id'],
			# 'name': instance['name'],
			# 'created_at': instance['created_at'],
			# 'image': instance['image'],
			# # 'members': self.members
			# }

    class Meta:
        model = Team
        fields = (
            'id',
			'name',
			'image',
            'members',
            'created_at'
            # 'members'
			)

	