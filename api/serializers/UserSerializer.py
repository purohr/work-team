from rest_framework import serializers
from api.models import User


class UserSerializer(serializers.ModelSerializer):
    #nombre = serializers.CharField()
    
    class Meta:
        model = User
        fields = [ 
                	'id',
                    'username',
                    'email',
                    ]
        	