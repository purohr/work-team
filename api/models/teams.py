from django.db import models
from api.models import User


class Team(models.Model):
    name = models.CharField('Nombre equipo', max_length =100)
    image = models.ImageField(upload_to="team_thumbnails", default="team_thumbnails/default.png")
    members = models.ManyToManyField(User, through='Member')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)   
    
    def __str__(self):
        return '{0} {1}'.format(self.name, self.members)   
