from django.db import models
from api.models import User, Team

class Member(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    team_id = models.ForeignKey(Team, on_delete=models.CASCADE)
    date_of_admission = models.DateTimeField(auto_now_add=True)
