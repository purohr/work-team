from django.urls import path

from api.views import TeamView, MemberView, UserView


urlpatterns = [
	# Urls Teams
    path('teams/', TeamView.Teams.as_view(), name = 'team_list'),
    path('teams/<int:team_id>/', TeamView.TeamDetail.as_view(), name = 'team_detail'),
    path('teams/<int:pk>/update/', TeamView.TeamUpdateView.as_view(), name = 'team_update_view'),
    path('teams/create/', TeamView.TeamCreateView.as_view(), name = 'team_create_view'),

    #Urls Member
    path('members/', MemberView.Members.as_view(), name = 'member_list'),
    path('members/<int:member_id>/', MemberView.MemberDetail.as_view(), name = 'member_detail'),
    path('members/<int:pk>/update/', MemberView.MemberUpdateView.as_view(), name = 'member_update_view'),
    path('members/create/', MemberView.MemberCreateView.as_view(), name = 'member_create_view'),

    #Urls User
	path('users/', UserView.Users.as_view(), name = 'user_list'),
    path('users/<int:user_id>/', UserView.UserDetail.as_view(), name = 'user_detail'),
    path('users/<int:pk>/update/', UserView.UserUpdateView.as_view(), name = 'user_update_view'),
    path('users/create/', UserView.UserCreateView.as_view(), name = 'user_create_view'),


]

"""
API Team
teams/
teams/id/
teams/id/members/
teams/id/members/id/
"""