from rest_framework.viewsets import ModelViewSet

from api.serializers import UserSerializer
from api.models.users import User

class UsersViewSet(ModelViewSet):
    """docstring for UsersViewSet"""
    serializer_class = UserSerializer
    queryset = User.objects.all()