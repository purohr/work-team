from rest_framework.viewsets import ModelViewSet

from api.serializers import MemberSerializer
from api.models.members import Member

class MembersViewSet(ModelViewSet):
    """docstring for MembersViewSet"""
    serializer_class = MemberSerializer
    queryset = Member.objects.all()


