from rest_framework.viewsets import ModelViewSet

from api.serializers import TeamSerializer
from api.models.teams import Team

class TeamsViewSet(ModelViewSet):
    """docstring for MembersViewSet"""
    serializer_class = TeamSerializer
    queryset = Team.objects.all()