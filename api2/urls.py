from django.urls import path

from api2.views import TeamsViewSet, MembersViewSet, UsersViewSet
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register(r"teams", TeamsViewSet)
router.register(r"members", MembersViewSet)
router.register(r"users", UsersViewSet)

urlpatterns = router.urls